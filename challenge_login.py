# /usr/bin/python3
import csv

# fonction login
def login(username, password):

    with open('accounts.csv', newline='') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=' ', quotechar='|')
        for userList in spamreader:
            arrayUser = ' '.join(userList)
            arrayUser = arrayUser.split(';')

            if username == 'user' and password == 'password':
                return False
            else:
                if username == arrayUser[0] and password == arrayUser[1]:
                    return True

    return False

"""
#tests
print("Toto knows his password ")
print(login('toto', 'challenge'))
# returns True

print("Paul knows his password")
print(login('paul', 'pole'))
# returns True

print("Toto forgot his password")
print(login('toto', 'falsepwd'))
# returns False

print("Tutu does not exist")
print(login('tutu', 'challenge'))
# returns False

print("User does not exist")
print(login('user', 'password'))
# returns False
"""