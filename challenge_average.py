# /usr/bin/python3

import csv
import statistics

# calculating the average
def average_score(username):

    with open('accounts.csv', newline='') as csvfile:
        spamreader = csv.DictReader(csvfile, delimiter=';', quotechar='|')
        for userList in spamreader:

            if userList['user'] == username:
                addScore = float(userList['score_0'])+float(userList[' score_1'])+float(userList['score_2'])+float(userList['score_3'])
                average = addScore / 4
                return average

    return False

"""
print("Toto has an average of 0")
print(average_score('toto'))
# returns 0

print("Tutu does not exist ")
print(average_score('tutu'))
# returns False

print("Paul has an average of 5,75")
print(average_score('paul'))
# returns 5.75

print("Jacques has an average of 6.25")
print(average_score('jacques'))
# returns 6.25
"""