# /usr/bin/python3
import csv

scoreArray = {}

# look for the maximum and the minimum for each user
def minmax():
    with open('accounts.csv') as accFile:
        accFile = csv.reader(accFile, delimiter=';')
        next(accFile)
        for row in accFile:
            usersList = row[0]
            userScores = row[2:len(row)]
            iUserScores = list(map(int, userScores))

            minScore = min(iUserScores)
            maxScore = max(iUserScores)

            minmaxScore = {}
            minmaxScore["min"] = minScore
            minmaxScore["max"] = maxScore

            scoreArray.update({usersList: {'min': minScore, 'max': maxScore}})

        return scoreArray

scores = minmax()


"""
print("Toto Maximum is 0")
print(scores['toto']['min'])
# returns 0

print("Paul Minimum is 3")
print(scores['paul']['min'])
# returns 3

print("Jacques Maximum is 8")
print(scores['jacques']['max'])
# returns 8

print("Pierre Maximum is 10")
print(scores['pierre']['max'])
# returns 10
"""
