
import unittest

from challenge_userExists import user_exists
from  challenge_login import login
from challenge_average import average_score
from  challenge_minmax import minimum
from  challenge_minmax import maximum
from challenge_minmaxuser import minmax



class challenge_userExists(unittest.TestCase):

    def test_userExist_toto(self):
        """ check if the user exists """
        self.assertTrue(user_exists('toto'))

    def test_userExist_tutu(self):
        """ check if the user exists """
        self.assertEqual(user_exists('tutu'), None)

    def test_userExist_user(self):
        """ check if the user exists """
        self.assertFalse(user_exists('user'))


class challenge_login(unittest.TestCase):

    def test_login_toto(self):
        """ test fonction login """
        self.assertTrue(login('toto', 'challenge'))

    def test_login_paul(self):
        """ test fonction login """
        self.assertTrue(login('paul', 'pole'))

    def test_login_toto_false(self):
        """ test fonction login """
        self.assertFalse(login('toto', 'falsepwd'))

    def test_login_tutu_false(self):
        """ test fonction login """
        self.assertFalse(login('tutu', 'challenge'))

    def test_login_user_false(self):
        """ test fonction login """
        self.assertFalse(login('user', 'password'))


class challenge_average(unittest.TestCase):

    def test_average_toto(self):
        """ test fonction average """
        self.assertEqual(average_score('toto'), 0.0)

    def test_average_tutu(self):
        """ test fonction average """
        self.assertFalse(average_score('tutu'))

    def test_average_paul(self):
        """ test fonction average """
        self.assertEqual(average_score('paul'), 5.75)

    def test_average_jacques(self):
        """ test fonction average """
        self.assertEqual(average_score('jacques'), 6.25)


class challenge_minmax(unittest.TestCase):

    def test_minimum(self):
        """ test fonction minimum """
        self.assertEqual(minimum(), 0)

    def test_maximum(self):
        """ test fonction maximum """
        self.assertEqual(maximum(), 10)


class challenge_minmaxuser(unittest.TestCase):

    def test_min_toto(self):
        """ test fonction min toto """
        scores = minmax()
        self.assertEqual(scores['toto']['min'], 0)

    def test_min_paul(self):
        """ test fonction min paul """
        scores = minmax()
        self.assertEqual(scores['paul']['min'], 3)

    def test_max_jacques(self):
        """ test fonction max jacques """
        scores = minmax()
        self.assertEqual(scores['jacques']['max'], 8)

    def test_max_pierre(self):
        """ test fonction max pierre """
        scores = minmax()
        self.assertEqual(scores['pierre']['max'], 10)


unittest.main()