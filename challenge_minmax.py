# /usr/bin/python3
import csv

allScores = []

# look for the minimum
def minimum():
    with open('accounts.csv', newline='') as csvfile:
        spamreader = csv.DictReader(csvfile, delimiter=';', quotechar='|')
        for scoreList in spamreader:
            allScores.append(int(scoreList['score_0']))
            allScores.append(int(scoreList[' score_1']))
            allScores.append(int(scoreList['score_2']))
            allScores.append(int(scoreList['score_3']))

    return min(allScores)

# look for the maximum
def maximum():
    with open('accounts.csv', newline='') as csvfile:
        spamreader = csv.DictReader(csvfile, delimiter=';', quotechar='|')
        for scoreList in spamreader:
            allScores.append(int(scoreList['score_0']))
            allScores.append(int(scoreList[' score_1']))
            allScores.append(int(scoreList['score_2']))
            allScores.append(int(scoreList['score_3']))

    return max(allScores)

"""
print("Minimum is 0")
print(minimum())
# returns 0


print("Maximum is 10")
print(maximum())
# returns 10
"""
