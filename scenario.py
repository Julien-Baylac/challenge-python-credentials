
# i import all my fonctions
from challenge_userExists import user_exists
from  challenge_login import login
from challenge_average import average_score
from  challenge_minmax import minimum
from  challenge_minmax import maximum
from challenge_minmaxuser import minmax

def scenario():

# i ask the name
    print('Enter your username : ')
    username = input()
    nameAnswer = user_exists(username)

# i ask the password and check if it match
    if nameAnswer == True:
        print('Hello '+ username + ', Enter your password : ')
        password = input()
        passwordAnswer = login(username, password)
        print('You are connected to the scoreboard. ')
        print('')

# i propose a choise into the average, his min and max score, and the min and max total score
        if passwordAnswer == True:
            print('If you want to know your average, type : 1 ')
            print('If you want to know your minimum and maximum score, type : 2 ')
            print('If you want to know the minimum and maximum total score, type : 3 ')

            choiseAnswer = input()

# the user choise and get an answer
            if choiseAnswer == '1':
                arevage = average_score(username)
                print('Your arevage is : ' + str(arevage) + '.')

            elif choiseAnswer == '2':
                scores = minmax()
                minscore = scores[username]['min']
                maxscore = scores[username]['max']
                print('Your minimum score is : ' + str(minscore) + '.')
                print('Your maximum score is : ' + str(maxscore) + '.')
            elif choiseAnswer == '3':
                minimumTotalScore = minimum()
                maximumTotalScore = maximum()
                print('The minimum total score is : ' + str(minimumTotalScore) + '.')
                print('The maximum total score is : ' + str(maximumTotalScore) + '.')

# if the answer isnt 1 2 or 3, the program says him that it dont understand him
            else:
                print('Sorry, i dont understand your answer.')

# when the program close, he say goodbye
    return print('Goodbye !')

print(scenario())
