# /usr/bin/python3
import csv

# fonction to verify if user exist
def user_exists(username):

    with open('accounts.csv', newline='') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=' ', quotechar='|')
        for userList in spamreader:
            arrayUser = ' '.join(userList)
            arrayUser = arrayUser.split(';')

#            print(arrayUser)

            if username == 'user':
                return False
            else:
                if username == arrayUser[0]:
                    return True

"""
# tests
print("Toto exists? ")
print(user_exists('toto'))
# returns True

print("Tutu exists? ")
print(user_exists('tutu'))
# returns False

print("user exists? ")
print(user_exists('user'))
# returns False
"""

